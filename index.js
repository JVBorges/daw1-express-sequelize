require('dotenv').config();
const db = require('./src/configs/sequelize');
const express = require('express');
const app = express();

app.use(express.static(`${__dirname}/public`));
app.use(express.json());

db.sequelize.sync({ alter: true }).then(() => console.log('DROP/CREATE TABLE COM SUCESSO!'));

const routes = require('./src/routes');

routes(app);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Servidor rodando na porta ${port}`));