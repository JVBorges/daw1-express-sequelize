const form = document.getElementById('commentForm');

form.onsubmit = async e => {
  e.preventDefault();

  const firstName = document.getElementById('inputFirstName').value;
  const lastName = document.getElementById('inputLastName').value;
  const content = document.getElementById('inputContent').value;

  const url = new URL('http://localhost:3000/post/salvar');

  try {
    const request = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ firstName, lastName, content })
    });

    const result = await request.json();

    createComment(result);
  } catch (err) {
    console.error(err);
  }
}

createComment = ({ id, content, updatedAt, user }) => {
  const comentarios = document.getElementById('comentarios');
  const userName = `${user.firstName} ${user.lastName}`;

  const comentario = `
  <figure class="my-2">
    <blockquote class="blockquote">
      <p id="p${id}">${content}</p>
      <input type="hidden" class="form-control" id="i${id}">
    </blockquote>
    <figcaption class="blockquote-footer">
      ${userName} - ${new Date(updatedAt).toLocaleDateString()}
    </figcaption>
  </figure>
  <i class="fas fa-edit" onclick="editComment(${id})"></i>
  <i class="fas fa-trash" onclick="removeComment(${id})"></i>
  `;
  comentarios.innerHTML += comentario.trim();
}

getCommments = async () => {
  const url = new URL('http://localhost:3000/post/listar');

  try {
    const request = await fetch(url);
    const result = await request.json();

    document.getElementById('comentarios').innerHTML = '';
    result.forEach(document => createComment(document));
  } catch (err) {
    console.error(err);
  }
}

editComment = id => {
  const inputEdit = document.getElementById(`i${id}`);
  const pContent = document.getElementById(`p${id}`);

  inputEdit.value = pContent.innerHTML;
  inputEdit.setAttribute('type', 'text');
  pContent.setAttribute('hidden', true);

  inputEdit.addEventListener('keyup', e => {
    if (e.key == 'Enter') saveEdit(inputEdit, pContent, id);
    if (e.key == 'Escape') changeCommentVisibility(inputEdit, pContent);
  });
}

changeCommentVisibility = (input, p) => {
  input.setAttribute('type', 'hidden');
  p.removeAttribute('hidden');
}

saveEdit = (input, p, id) => {
  const url = new URL('http://localhost:3000/post/editar');

  fetch(url, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ content: input.value, id })
  }).then(res => res.json())
    .then(data => p.innerHTML = input.value)
    .catch(err => {
      console.error(err);
    });

  this.changeCommentVisibility(input, p);
  this.getCommments();
}

removeComment = id => {
  const url = new URL('http://localhost:3000/post/remover');

  fetch(url, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ id })
  }).then(res => res.json())
    .then(data => console.log(data))
    .catch(err => {
      console.error(err);
    });

  this.getCommments();
}

window.onload = () => this.getCommments();