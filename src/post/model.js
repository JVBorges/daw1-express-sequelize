const { Model, DataTypes } = require('sequelize');
const { sequelize } = require('../configs/sequelize');
const { model: User } = require('../user');

class Post extends Model { }

Post.init({
  content: DataTypes.STRING
}, { sequelize, modelName: 'posts' });

Post.User = Post.belongsTo(User);

module.exports = Post;