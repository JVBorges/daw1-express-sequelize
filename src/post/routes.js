const controller = require('./controller');

module.exports = app => {
  app.post('/post/salvar', controller.salvar);
  app.get('/post/listar', controller.listar);
  app.put('/post/editar', controller.update);
  app.delete('/post/remover', controller.remove);
}