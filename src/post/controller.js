const Post = require('./model');
const { model: User } = require('../user');

module.exports = {
  salvar: async (req, res) => {
    const { content, firstName, lastName } = req.body;
    const user = { firstName, lastName };

    try {
      const result = await Post.create({ content, user }, {
        include: [
          { association: Post.User }
        ]
      });

      res.status(201).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao salvar o post' });
    }
  },

  listar: async (req, res) => {
    try {
      const result = await Post.findAll({ include: User });

      res.status(200).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao listar os posts' });
    }
  },

  update: async (req, res) => {
    const { content, id } = req.body;

    try {
      const result = await Post.update({ content }, { where: { id } });

      res.status(200).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao editar o post' });
    }
  },

  remove: async (req, res) => {
    const { id } = req.body;

    try {
      const result = await Post.destroy({ where: { id } });

      res.status(200).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao editar o post' });
    }
  }
}