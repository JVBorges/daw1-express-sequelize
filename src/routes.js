const path = require('path');
const user = require('./user');
const post = require('./post');

module.exports = app => {
  app.get('/', (req, res) => res.sendFile(path.join(__dirname, '../public/views/index.html')));

  user.routes(app);
  post.routes(app);
}