const { Model, DataTypes } = require('sequelize');
const { sequelize } = require('../configs/sequelize');

class User extends Model { }

User.init({
  firstName: DataTypes.STRING,
  lastName: DataTypes.STRING,
}, { sequelize, modelName: 'users' });

module.exports = User;