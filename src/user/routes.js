const controller = require('./controller');

module.exports = app => {
  app.post('/user/salvar', controller.salvar);
  app.get('/user/listar', controller.listar);
}