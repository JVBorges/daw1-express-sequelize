const User = require('./model');

module.exports = {
  salvar: async (req, res) => {
    const { firstName, lastName } = req.body;

    try {
      const result = await User.create({ firstName, lastName });

      res.status(201).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao salvar usuário' });
    }
  },

  listar: async (req, res) => {
    try {
      const result = await User.findAll();

      res.status(200).json(result);
    } catch (err) {
      res.status(500).json({ message: 'Erro ao listar os usuários' });
    }
  }
}