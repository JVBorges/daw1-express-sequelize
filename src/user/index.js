module.exports = {
  controller: require('./controller'),
  model: require('./model'),
  routes: require('./routes')
}