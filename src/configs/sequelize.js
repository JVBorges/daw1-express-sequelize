const config = require('./database');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(config);

const db = { Sequelize, sequelize };

module.exports = db;